# Backyard Tools: Overture
## Bits and bobs

This repository contains useful or supplementary files and documents pertaining to `Guilty Gear 2: Overture` that don't fit anywhere else.

### Files in this repo

`unitstat.csv` - A CSV conversion of the unused `data/settings/unitstat.slk` file shipped with the game. Due to the SYLK format traditionally only supporting ANSI, and `unitstat.slk` being Shift-JIS encoded, some tools cannot display it correctly. This file is a Shift-JIS encoded CSV conversion which displays the text correctly.